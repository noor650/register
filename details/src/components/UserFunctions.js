// api calls for login, register
import axios from 'axios';
 export const register= newuser =>{
     return axios
     .post('http://localhost:3004/users/register',{
        first_name:newuser.first_name,
        last_name:newuser.last_name,
        email:newuser.email,
        password:newuser.password,

     }).then(res=>{
        console.log("Registered");
     });
 }

 export const login= user =>{
    return axios
    .post('http://localhost:3004/users/login',{
       email:user.email,
       password:user.password,

    }).then(res=>{
        localStorage.setItem('usertoken',res.data)
       return res.data
    }).catch(err=>{
        console.log(err)
    });
}