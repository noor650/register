// import React, { Component } from 'react';
import React from 'react';
import axios from 'axios';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Navbar from './components/Navbar'
import Landing from './components/Landing'
import Login from './components/Login'
import Register from './components/Register'
import Profile from './components/Profile'
// import logo from './logo.svg';
import './App.css';


class UserDetails extends React.Component{
	constructor(props){
		 super(props);
		 this.state = { 
		name:'',
		 email: '',
		 password:'',
		 address:'',
		 city:'',
		 state:'',
		 zip:'',
		 };
	 	

	 }
	submitEvent = (event) => {
		event.preventDefault();
			axios({
				method: 'post',
				url: 'http://127.0.0.1:3004/users/',
				data: this.state
				})
			.then(function (response) {
				//handle success
				console.log(response.data.errors);
			})
			.catch(function (response) {
				//handle error
				console.log(response.data.errors);
			});
	  }
	  onNameChange(event) {
		this.setState({name: event.target.value})
	  }
	  onEmailChange(event) {
		this.setState({email: event.target.value})
	  }
	
	  onPasswordChange(event) {
		this.setState({password: event.target.value})
	  }
	  onAddressChange(event) {
		this.setState({address: event.target.value})
	  }
	  onCityChange(event) {
		this.setState({city: event.target.value})
	  }
	  onStateChange(event) {
		this.setState({state: event.target.value})
	  }
	  onZipChange(event) {
		this.setState({zip: event.target.value})
	  }
	render(){
		return(
		<form onSubmit={this.submitEvent}>
		  <div className="form-row" >
		    <div className="form-group col-md-6">
		      <label>Name</label>
		      <input type="text" className="form-control" value={this.state.name}   onChange={this.onNameChange.bind(this)}/>
		    </div>

		    <div className="form-group col-md-6">
		      <label>Email</label>
		      <input type="email" className="form-control" value={this.state.email}   onChange={this.onEmailChange.bind(this)} placeholdertext="example@abc.com"/>
		    </div>
		  </div>
		  <div className="form-group">
		  <div className="form-group col-md-6">
		      <label>Password</label>
		      <input type="password" className="form-control"  value={this.state.password} onChange={this.onPasswordChange.bind(this)} placeholdertext={"******"}/>
		    </div>
      <div className="form-group col-md-6">
		    <label>Address</label>
		    <input type="text" className="form-control"  value={this.state.address} onChange={this.onAddressChange.bind(this)} placeholdertext={"147 st."}/>
		  </div>
      </div>
		
		  <div className="form-row">
		    <div className="form-group col-md-6">
		      <label>City</label>
		      <input type="text" className="form-control" value={this.state.city} onChange={this.onCityChange.bind(this)} />
		    </div>
		    <div className="form-group col-md-4">
		      <label>State</label>
		      <select value={this.state.state} className="form-control" onChange={this.onStateChange.bind(this)}>
		        <option value="">Choose...</option>
		        <option value="usa">USA</option>
        		<option value="ca">CA</option>
        		<option value="in">IN</option>
		      </select>
		    </div>
		    <div className="form-group col-md-2">
		      <label>Zip</label>
		      <input type="text" className="form-control" value={this.state.zip} onChange={this.onZipChange.bind(this)}/>
		    </div>
		  </div>
      <div className="form-group col-md-12">
		  <button type="submit" className="btn btn-primary">Sign in</button>
      </div>
		</form>
		);
	}
}
export default UserDetails;


