const express = require('express');
const cors = require('cors');
// const jwt = require('express-jwt');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const router = express.Router();
var userModels  = require('../models').userDetails;
var User  = require('../models').User;
process.env.SECRET_KEY="secret"
'use strict';

router.get('/', function(req, res, next) {
  // res.render('index', { title: 'Express' });
});

router.post('/users', function(req, res, next) {
    const user = userModels.findOne({ where: {'email':req.body.email}}).then(user=>{
      if(!user){
        bcrypt.hash(req.body.password,10,(err,hash)=>{
            req.body.password=hash;
            const response= userModels.create(req.body).then(user => {
              res.json({status: user.email + ' registered'})
            })
          .catch((err) => {
            res.send('error:' + err);
          });
        });
          
      }else{
        res.json({error:"User already exists"})
      }
    }).catch((err) => {
      res.send('error:' + err);
    });
});

router.post('/login',function(req,res,next){
  const user = User.findOne({
     where: {
       'email':req.body.email
      }
    })
    .then(user=>{
    if(user){
      if(bcrypt.compareSync(req.body.password,user.password)){
       let token= jwt.sign(user.dataValues,process.env.SECRET_KEY,{
          expiresIn:1440
        })
        console.log(token)
        res.send(token)
      }
    }else{
      res.status(404).json({error:"User doesn't exists"})
    }
}).catch((err) => {
  res.status(404).json({error: err})
})
});



router.post('/register',function(req,res,next){
  const userData={
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password,
  }
  const user = User.findOne({
     where: {
       'email':req.body.email
      }
    })
    .then(user=>{
      if(!user){
        bcrypt.hash(req.body.password,10,(err,hash)=>{
            req.body.password=hash;
            const response= User.create(req.body).then(user => {
              res.json({status: user.email + ' registered'})
            })
          .catch((err) => {
            res.send('error:' + err);
          });
        });
          
      }else{
        res.json({error:"User already exists"})
      }
}).catch((err) => {
  res.status(404).json({error: err})
});
});

module.exports = router;
