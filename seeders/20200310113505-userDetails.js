'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
   return queryInterface.bulkInsert('userDetails', [{
    id:0,
    name: 'noor',
    email: 'exampl@example.com',
    password:'password',
    address: "st john ",
    city: "singapor",
    state: "jammu and kashmir",
    zip: "193221",
    createdAt: new Date(),
    updatedAt: new Date()
}]);
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   return queryInterface.bulkDelete('userDetails', null, {});
  }
};
