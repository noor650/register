'use strict';
module.exports = (sequelize, DataTypes) => {
  const userDetails = sequelize.define('userDetails', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    address: DataTypes.TEXT,
    city: DataTypes.TEXT,
    state: DataTypes.STRING,
    zip: DataTypes.STRING,
  }, {});
  userDetails.associate = function(models) {
    // associations can be defined here
  };
  return userDetails;
};